angular
.module('app.users.admin',[])
.controller('UsersCtrl', function ($scope, $uibModal, $webSql, toastr) {

  $scope.db = $webSql.openDatabase('komodo', '1.0', 'Komodo DB', 2 * 1024 * 1024);

  $scope.readUsers = function () {
    $scope.db.selectAll("users").then(function(results) {
      $scope.usersData = [];
      for(var i=0; i < results.rows.length; i++){
        $scope.usersData.push(results.rows.item(i));
      }
    });
  };

  $scope.open = function (size, user) {
    var modalInstance =   $uibModal.open({
      animation: false,
      controller: 'ModalInstanceCtrl',
      templateUrl: 'views/users/addUser.html',
      scope: $scope,
      size: size,
      resolve: {
        user: function () {
          return {user: user};
        }
      }
    });
    modalInstance.result.then(function () {

    }, function (data) {
      console.log(data);
      if(data == "update"){
        $scope.readUsers();
      }

    });
  };

  $scope.delete = function (ci) {
    var r= confirm('¿Realmente desea elimiar el usuario?');
    if(r){
      $scope.db.del("users", {"ci": ci});
      $scope.readUsers();
    }
  };
})
.controller('ModalInstanceCtrl', function ($uibModalInstance, $scope, $webSql, toastr, user) {
  $scope.toastr = toastr;
  $scope.userData = {};

  if(angular.isDefined(user.user.ci)){
     angular.copy(user.user, $scope.userData);
  }else{
    $scope.userData = {document:"c", nationality:"v", type: "employee"};
    $scope.new = true;
  }
  $scope.db = $webSql.openDatabase('komodo', '1.0', 'Komodo DB', 2 * 1024 * 1024);

  $scope.addUser = function () {
    $scope.db.insert('users', $scope.userData)
    .then(function(results) {
      console.log(results);
      $uibModalInstance.dismiss('update');
    },function (err) {
      if(err.code === 6){
        $scope.toastr.error("El número de cédula ya se encuentra registrado en el sistema");
      }
      console.log(err);
    });

  };

  $scope.update = function () {
    console.log($scope.userData.ci);
    $scope.db.update("users", $scope.userData, { 'ci' : ''+$scope.userData.ci+'' });
    $uibModalInstance.dismiss('update');
  };

  $scope.cancel = function () {
    $uibModalInstance.dismiss('cancel');
  };
});
