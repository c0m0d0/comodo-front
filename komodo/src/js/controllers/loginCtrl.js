angular
.module('app.login',[])
.controller('loginCtrl', function ($scope, $rootScope, $webSql, toastr, $state, toastr, $localStorage) {

  $scope.db = $webSql.openDatabase('komodo', '1.0', 'Komodo DB', 2 * 1024 * 1024);
  $scope.loginData = {};
  $scope.usersData = [];
  $scope.storage = $localStorage;
  $scope.readLogin = function () {
    $scope.db.selectAll("users").then(function(results) {
      for(var i=0; i < results.rows.length; i++){
        $scope.usersData.push(results.rows.item(i));
      }
    });
  };

  $scope.login = function () {
    var login = false;
    // console.log($scope.usersData);
    angular.forEach($scope.usersData, function (value, key) {
      console.log(value.ci == $scope.loginData.ci && value.password == $scope.loginData.password);
      if(value.ci == $scope.loginData.ci && value.password == $scope.loginData.password){
        login = true;
        $scope.storage.login = true;
        $scope.storage.userType = value.type;
        $rootScope.userType = value.type;
      }
    })

    if(login == true){
      console.log("LOGIN");
      $state.go("app.main")
    }else{
      toastr.error("Credenciales Invalidas");
    }
  }
})
